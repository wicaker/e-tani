const Payment = require("../models/Payment");
require('dotenv').config();

module.exports = async (req, resp) => {
  const reqHeaderToken = req.headers['x-callback-token'];

  if (reqHeaderToken !== process.env.XENDIT_CALLBACK_VERIFICATION_KEY) {
    resp.status(401).send({
      message: 'Unauthorized'
    });
    return;
  }

  try {
    const {
      external_id,
      status,
      payment_method,
      bank_code
    } = req.body;

    let method = 'CREDIT CARD';
    if (payment_method == 'POOL') {
      method = `VA - ${bank_code}`
    }

    const payment = await Payment.findOne({external_id});
    
    if (payment) {
      await payment.update({
        status,
        payment_method: method
      })
    }

    resp.send('ok')
  } catch (error) {
    console.log(error);
    throw error;
  }
};
