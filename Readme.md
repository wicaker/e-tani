# Backend For E-Tani 

1. Clone the repository
2. `npm install` first to install all depedency
3. rename `.env.example` to `.env`

### Folder Structure
1. `middleware` contain file to authentication
2. `models` contain file database models
3. `public` contain images of product
4. `routes` routing endpoint


### Depedency
- bcrypt : For bcrypt or hashing password, so user password secure in database
- dotenv : for env setting
- express: Framework forn Node Js
- jsonwebtoken : Create token when user login
- mongoose : ORM (Object-relational mapping) of mongodb models
- multer: midleware for request file


### Endpoint
- `/api/user/`, `/api/profile/`, `/api/product/`, `/api/transaction/`
- User Login and Register in folder `./routes/user`
- Add Profile (user must create profile, before they can sell a product) in folder `./routes/profile`
- CRUD for product `./routes/product`
- Transaction (buy product or add to cart) `./routes/transaction`
