const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

//Load User Models
const User = require("../models/User");

// @route POST api/user/register
// @desc Register User
// @access public
router.post("/register", async (req, res) => {
  if (!req.body.email) {
    return res.status(400).json({ "error": "Email required!" });
  }
  if (!req.body.password) {
    return res.status(400).json({ "error": "Password required!" });
  }
  //check email, turn error if email already exist
  const getUser = await User.findOne({ email: req.body.email });
  if (getUser) {
    return res.status(400).json({ "error": "Email already exists!" });
  }
  bcrypt.hash(req.body.password, 10, function (err, hash) {
    User
      .create({
        email: req.body.email,
        password: hash,
        status: req.body.status
      })
      .then(() => res.status(200).json({ "message": "You successfully register, please login first!" }))
      .catch(err => { throw new Error(err) })
  });
})


// @route POST api/user/login
// @desc login User
// @access public
router.post("/login", async (req, res) => {
  if (!req.body.email) {
    return res.status(400).json({ "error": "Email required!" });
  }
  if (!req.body.password) {
    return res.status(400).json({ "error": "Password required!" });
  }

  //Find user by email
  const getUser = await User.findOne({ email: req.body.email });
  if (!getUser) {
    return res.status(400).json({ "error": "User Not Found!" });
  }

  //check password
  bcrypt.compare(req.body.password, getUser.password, function (err, status) {
    if (err) {
      throw new Error(err);
    }
    if (!status) {
      return res.status(400).json({ "error": "Password invalid!" });
    }
    const token = jwt.sign({ user_id: getUser._id, email: getUser.email, status: getUser.status || 'non-admin' }, 'secretkey', { expiresIn: 86400 }); //expired in 24 hours
    return res.status(200).json({ "token": 'Bearer ' + token });
  });
})

module.exports = router;