const express = require('express');
const router = express.Router();
require('dotenv').config()
const auth = require("../middleware/auth");


//Load Models
const Payment = require("../models/Payment");

// @route GET /api/payments
// @desc GET all status payments
// @access private
router.get('/', auth, async (req, res) => {
  if (!req.isAuth) {
    return res.status(400).json({ "error": "Unauthenticated!" });
  }
  try {
    if (req.user.status == 'admin') {
      let payments = await Payment.find();
      if (payments.length == 0) {
        return res.status(400).json({ "message": "No Payment found!" })
      }
      return res.status(200).json({data:payments});
    } else {
      let payments = await Payment.find({buyer_id:req.user.user_id});
      if (payments.length == 0) {
        return res.status(400).json({ "message": "No Payment found!" })
      }
      return res.status(200).json({data:payments});
    }
  } catch (error) {
    return res.status(500).json({ "error": error.message })
  }
})

module.exports = router;