const express = require('express');
const router = express.Router();
const auth = require("../middleware/auth");

//Load Models
const Profile = require("../models/Profile");

// @route GET /api/profile
// @desc look profile of user
// @access private
router.get('/', auth, (req, res) => {
  if (!req.isAuth) {
    return res.status(400).json({ "error": "Unauthenticated!" });
  }
  Profile
    .findOne({ user_id: req.user.user_id })
    .then(data => res.status(200).json(data))
    .catch(err => new Error(err));
});

// @route POST api/profile/create
// @desc create new profile
// @access private
router.post('/create', auth, async (req, res) => {
  if (!req.isAuth) {
    return res.status(400).json({ "error": "Unauthenticated!" });
  }
  const getProfile = await Profile.findOne({ user_id: req.user.user_id });
  if (getProfile) {
    return res.status(400).json({ "error": "Profile already created, try edit to edit profile" })
  }
  Profile
    .create({
      user_id: req.user.user_id,
      name: req.body.name,
      phone_number: req.body.phone_number,
      address: req.body.address,
      city: req.body.city,
      rekening_name1: req.body.rekening_name1,
      rekening_number1: req.body.rekening_number1,
      rekening_name2: req.body.rekening_name2,
      rekening_number2: req.body.rekening_number2,
    })
    .then(() => res.status(200).json({ "message": "Profile successfully created!" }))
    .catch(err => { throw new Error(err); });
});

// @route PUT api/profile/edit
// @desc update user profile
// @access private
router.put('/', auth, (req, res) => {
  if (!req.isAuth) {
    return res.status(400).json({ "error": "Unauthenticated!" });
  }
  Profile
    .findOneAndUpdate({ user_id: req.user.user_id }, req.body)
    .then(() => res.status(200).json({ "message": "Profile successfully updated!" }))
    .catch(err => new Error(err));
});

// @route DELETE api/profile/delete
// @desc delete user profile
// @access private
router.delete('/:profile_id', auth, (req, res) => {
  if (!req.isAuth) {
    return res.status(400).json({ "error": "Unauthenticated!" });
  }
  Profile
    .findByIdAndDelete(req.params.profile_id)
    .then((data) => {
      if (!data) {
        return res.status(400).json({ "message": "Profile not found!" })
      }
      return res.status(200).json({ "message": "Profile successfully deleted!" })
    })
    .catch(err => new Error(err));
});



module.exports = router;