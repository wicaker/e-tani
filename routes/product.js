const express = require('express');
const router = express.Router();
const auth = require("../middleware/auth");
const fs = require("fs");
const multer = require("multer");
const mongoose = require('mongoose');


//storage image
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'public/uploads/product/images/');
  },
  filename: function (req, file, cb) {
    cb(null, new Date().toISOString() + '-product-' + file.originalname);
  }
})

const fileFilter = (req, file, cb) => {
  // reject a file
  if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png' || file.mimetype === 'image/jpg') {
    cb(null, true);
  } else {
    cb(null, false);
  }
}
const upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 5 //max 5Mb
  },
  fileFilter: fileFilter
});

//Load Models
const Product = require("../models/Product");
const Profile = require("../models/Profile");

// @route GET /api/product
// @desc look all product
// @access public
router.get("/", auth, async (req, res, next) => {
  if (!req.isAuth) {
    return res.status(400).json({ "error": "Unauthenticated!" });
  }
  let getProducts = await Product.find();
  if (!getProducts) {
    return res.status(400).json({ "message": "Product not found!" })
  }
  getProducts = getProducts.map(item => {
    return { ...item._doc, productImage: req.headers.host + '/' + item.productImage }
  })
  return res.status(200).json(getProducts);
})

// @route GET /api/product/:category
// @desc look all product by category
// @access public
router.get("/category/:category", auth, async (req, res, next) => {
  if (!req.isAuth) {
    return res.status(400).json({ "error": "Unauthenticated!" });
  }
  let getProducts = await Product.find({ category: req.params.category });
  if (!getProducts) {
    return res.status(400).json({ "message": "Product not found!" })
  }
  getProducts = getProducts.map(item => {
    return { ...item._doc, productImage: req.headers.host + '/' + item.productImage }
  })
  return res.status(200).json(getProducts);
})


// @route GET /api/product/etani/:idProduct
// @desc look a product by Id
// @access public
router.get("/:idProduct", auth, async (req, res, next) => {
  if (!req.isAuth) {
    return res.status(400).json({ "error": "Unauthenticated!" });
  }
  const getProduct = await Product.findById(req.params.idProduct);
  if (!getProduct) {
    return res.status(400).json({ "message": "Product not found!" })
  }
  return res.status(200).json({ ...getProduct._doc, productImage: req.headers.host + '/' + getProduct.productImage });
})

// @route POST /api/product
// @desc add new Product
// @access private
router.post("/", auth, upload.single('product_image'), async (req, res, next) => {
  if (!req.isAuth) {
    return res.status(400).json({ "error": "Unauthenticated!" });
  }
  if (!req.file) {
    return res.status(400).json({ "error": "File type must be jpg/jpeg/png" })
  }
  const getSellerData = await Profile.findOne({ user_id: req.user.user_id });
  if (!getSellerData) {
    return res.status(400).json({ "error": "Please add profile before sell product!" });
  }
  const product = new Product({
    _id: new mongoose.Types.ObjectId(),
    seller: req.user.user_id,
    name: req.body.name,
    price: req.body.price,
    stock: req.body.stock,
    category: req.body.category,
    weight: req.body.weight,
    description: req.body.description,
    productImage: req.file.path
  })
  product
    .save()
    .then(() => res.status(200).json({ "message": "Product successfully added!" }))
    .catch(err => new Error(err));
});

// @route DELETE /api/product/delete/:idProduct
// @desc delete a Product
// @access private
router.delete('/delete/:idProduct', auth, (req, res) => {
  if (!req.isAuth) {
    return res.status(400).json({ "error": "Unauthenticated!" });
  }
  Product
    .findByIdAndDelete(req.params.idProduct)
    .then((data) => {
      if (!data) {
        return res.status(400).json({ "message": "Product not found!" })
      }
      fs.unlinkSync(data.productImage);
      return res.status(200).json({ "message": "Product successfully deleted!" })
    })
    .catch(err => new Error(err));
})

// @route PUT api/user/product/edit
// @desc update a product
// @access private
router.put('/:idProduct', auth, upload.single('product_image'), (req, res) => {
  if (!req.isAuth) {
    return res.status(400).json({ "error": "Unauthenticated!" });
  }
  Product
    .findOneAndUpdate({ _id: req.params.idProduct }, { ...req.body, productImage: req.file.path })
    .then(data => {
      if (!data) {
        return res.status(400).json({ "message": "Product not found!" });
      }
      fs.unlinkSync(data.productImage);
      return res.status(200).json({ "message": "Product successfully updated!" })
    })
    .catch(err => new Error(err));
});


module.exports = router;