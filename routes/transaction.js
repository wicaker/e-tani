const express = require("express");
const router = express.Router();
require("dotenv").config();
const auth = require("../middleware/auth");
const request = require("request-promise");
const xendit_invoice_endpoint = "https://api.xendit.co/v2/invoices";
const apikey = Buffer.from(`${process.env.XENDIT_SECRET_API_KEY}:`).toString(
  "base64"
);

//Load Models
const Product = require("../models/Product");
const Cart = require("../models/Cart");
const User = require("../models/User");
const Profile = require("../models/Profile");
const Payment = require("../models/Payment");

// @route POST /api/transaction/add_cart
// @desc add to cart list
// @access private
router.post("/add_cart", auth, async (req, res) => {
  if (!req.isAuth) {
    return res.status(400).json({ error: "Unauthenticated!" });
  }
  
  const getProduct = await Product.findById(req.body.product_id);
  if (!getProduct) {
    return res.status(400).json({ message: "Product not found!" });
  }

  await Cart.create({
    user_id: req.user.user_id,
    product_id: req.body.product_id,
    product_name: getProduct.name,
    amount: req.body.amount
  });

  res.status(200).json({ message: "Product successfully added to cart!" });
});

// @route GET /api/transaction/cart
// @desc check to cart list
// @access private
router.get("/carts", auth, (req, res) => {
  if (!req.isAuth) {
    return res.status(400).json({ error: "Unauthenticated!" });
  }
  Cart.find({ user_id: req.user.user_id })
    .then(data => res.status(200).json(data))
    .catch(err => {
      throw new Error(err);
    });
});

// @route GET /api/transaction/cart/:idCart
// @desc check cart by id
// @access private
router.get("/cart/:idCart", auth, (req, res) => {
  if (!req.isAuth) {
    return res.status(400).json({ error: "Unauthenticated!" });
  }
  Cart.findById(req.params.idCart)
    .then(data => res.status(200).json(data))
    .catch(err => {
      throw new Error(err);
    });
});

// @route PATCH /api/transaction/cart/payments
// @desc payment all product in cart
// @access private
router.patch("/cart/payments", auth, async (req, res) => {
  try {
    if (!req.isAuth) {
      return res.status(400).json({ error: "Unauthenticated!" });
    }
    let data = {
      product: [],
      total: 0
    };

    for (i = 0; i < req.body.cart_id.length; i++) {
      const cart = await Cart.findById(req.body.cart_id[i]);
      const product = await Product.findById(cart.product_id);
      data.product.push(product.name);
      data.total += product.price * cart.amount;
    }

    const external_id = `invoice-${Math.floor(new Date() / 1000)}-${
      req.user.user_id
    }`;
    const invoice = await request.post({
      url: xendit_invoice_endpoint,
      json: true,
      resolveWithFullResponse: true,
      headers: {
        Authorization: `Basic ${apikey}`
      },
      body: {
        external_id: external_id,
        payer_email: req.user.email,
        description: `transaction of product : ${data.product.toString()}`,
        amount: data.total
      }
    });

    if (invoice.statusCode !== 200) {
      throw new Error("Payment failed, Something went wrong!");
    }

    // if invoice request success, create payment data
    const payment = await Payment.create({
      // product: "cart",
      amount: data.total,
      buyer_id: req.user.user_id,
      status: invoice.body.status,
      invoice_id: invoice.body.id,
      invoice_url: invoice.body.invoice_url,
      external_id: external_id,
      payment_method: "xendit-invoice"
    });

    // delete chart
    for (i = 0; i < req.body.cart_id.length; i++) {
      const cart = await Cart.remove({ _id: req.body.cart_id[i] });
    }

    return res.status(200).json(payment);
  } catch (error) {
    return res.status(500).json({ error: error.message });
  }
});

// @route DELETE /api/transaction/carts
// @desc delete cart
// @access private
router.delete("/carts", auth, async (req, res) => {
  try {
    if (!req.isAuth) {
      return res.status(400).json({ error: "Unauthenticated!" });
    }

    // delete cart
    for (i = 0; i < req.body.cart_id.length; i++) {
      await Cart.remove({
        _id: req.body.cart_id[i],
        user_id: req.user.user_id
      });
    }

    return res.status(200).json({ message: "Cart sucessfully deleted" });
  } catch (error) {
    return res.status(500).json({ error: error.message });
  }
});

// @route POST /api/transaction/buy
// @desc buy product
// @access public
router.post("/buy", auth, async (req, res) => {
  if (!req.isAuth) {
    return res.status(400).json({ error: "Login to buy product!" });
  }
  try {
    const getProduct = await Product.findById(req.body.product_id);
    if (!getProduct) {
      return res.status(400).json({ error: "Product not found!" });
    }
    const sellerData = await Profile.findOne({ user_id: getProduct.seller });
    if (!sellerData) {
      return res.status(400).json({ error: "No profile found!" });
    }

    const data = {
      product: getProduct.name,
      amount: req.body.amount,
      total: req.body.amount * getProduct.price,
      seller: sellerData.name,
      rekening: {
        bank1: sellerData.rekening_name1,
        bank2: sellerData.rekening_name2,
        number1: sellerData.rekening_number1,
        number2: sellerData.rekening_number2
      }
    };
    const external_id = `invoice-${Math.floor(new Date() / 1000)}-${
      req.user.user_id
    }`;
    const invoice = await request.post({
      url: xendit_invoice_endpoint,
      json: true,
      resolveWithFullResponse: true,
      headers: {
        Authorization: `Basic ${apikey}`
      },
      body: {
        external_id: external_id,
        payer_email: req.user.email,
        description: `transaction of product : ${data.product}`,
        amount: data.total
      }
    });

    if (invoice.statusCode !== 200) {
      throw new Error("Payment failed, Something went wrong!");
    }

    // if invoice request success, create payment data
    const payment = await Payment.create({
      product: req.body.product_id,
      amount: data.total,
      buyer_id: req.user.user_id,
      status: invoice.body.status,
      invoice_id: invoice.body.id,
      invoice_url: invoice.body.invoice_url,
      external_id: external_id,
      payment_method: "xendit-invoice"
    });

    return res.status(200).json(payment);
  } catch (error) {
    return res.status(500).json({ error: error.message });
  }
});

module.exports = router;
