const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const cartSchema = new Schema(
  {
    user_id: {
      type: Schema.Types.ObjectId,
      ref: "User"
    },
    product_id: {
      type: Schema.Types.ObjectId,
      ref: "Product"
    },
    product_name: {
      type: String,
    },
    amount: {
      type: Number,
      required: true
    },
  },
  { timestamps: true } //mongoose automaticaly add createdAt and updatedAt
);

module.exports = mongoose.model("Cart", cartSchema);
