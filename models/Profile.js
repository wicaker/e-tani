const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const profileSchema = new Schema(
  {
    user_id: {
      type: Schema.Types.ObjectId,
      ref: "User"
    },
    name: {
      type: String,
      required: true
    },
    phone_number: {
      type: String,
      required: true
    },
    address: String,
    city: String,
    rekening_name1: String,
    rekening_number1: String,
    rekening_name2: String,
    rekening_number2: String,

    // number_rekening1: String,
    // number_rekening2: String
  },
  { timestamps: true } //mongoose automaticaly add createdAt and updatedAt
);

module.exports = mongoose.model("Profile", profileSchema);
