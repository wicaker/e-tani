const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const userSchema = new Schema(
  {
    email: {
      type: String,
      required: true
    },
    password: {
      type: String,
      required: true
    },
    status: {
      type: String,
      required: false
    }
  },
  { timestamps: true } //mongoose automaticaly add createdAt and updatedAt
);

module.exports = mongoose.model("User", userSchema);
