const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const productSchema = new Schema(
  {
    seller: {
      type: Schema.Types.ObjectId,
      ref: "User"
    },
    name: {
      type: String,
      required: true
    },
    price: {
      type: Number,
      required: true,
    },
    stock: {
      type: Number,
      required: true
    },
    category: String,
    description: String,
    weight: String,
    productImage: {
      type: String,
      required: true
    },
  },
  { timestamps: true } //mongoose automaticaly add createdAt and updatedAt
);

module.exports = mongoose.model("Product", productSchema);
