const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const paymentSchema = new Schema(
  {
    product: {
      type: Schema.Types.ObjectId,
      ref: "Product"
    },
    amount: {
      type: Number,
      required: true
    },
    buyer_id : {
      type: Schema.Types.ObjectId,
      ref: "User"
    },
    status: {
      type: String,
      required: true
    },
    external_id: {
      type: String,
      required: true,
    },
    invoice_id:{
      type:String,
      required: true,
    },
    invoice_url:{
      type: String,
      required: true,
    },
    payment_method: {
      type: String,
    }
  },
  { timestamps: true } //mongoose automaticaly add createdAt and updatedAt
);

module.exports = mongoose.model("Payment", paymentSchema);
