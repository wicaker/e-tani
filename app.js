const express = require("express");
const app = express();
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
// const passport = require("passport");
const users = require("./routes/user");
const profile = require("./routes/profile");
const product = require("./routes/product");
const payment = require("./routes/payment");
const transaction = require("./routes/transaction");
require('dotenv').config()

//Body parser midleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//Connet to MongoDB
mongoose
  .connect(`mongodb+srv://${process.env.MONGO_USERNAME}:${process.env.MONGO_PASSWORD}@cluster0-wa3nj.mongodb.net/test?retryWrites=true`, { useNewUrlParser: true })
  .then(() => console.log("MongoDB Connected"))
  .catch(err => console.log(err));

//Avoid Access-Control-Allow-Origin Error
app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*"); //this allow all client with everything domain to access our api
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, PUT, PATCH, DELETE"
  );
  res.setHeader("Access-Control-Allow-Headers", "Content-Type, Authorization");
  next();
});

//xendit invoice callback
const paymentCallback = require('./payments/xendit-callback')
app.use('/invoice', paymentCallback)

//Use Routes
app.use("/api/user", users);
app.use("/api/profile", profile);
app.use("/api/product", product);
app.use("/api/transaction", transaction);
app.use("/api/payments", payment);


//serve static file images
app.use('/public/uploads/product/images', express.static('public/uploads/product/images'))


const port = process.env.PORT || 5000;
//connect to server
app.listen(port, () =>
  console.log(`Server running on port ${port}`)
);

